const http = require('http');
const port = 3000;
const controllers = require('./controllers');
const express = require('express');

const config = require('./config/config');
let enviroment = process.env.NODE_ENV || 'development';
let app = express();

require('./config/database.config')(config[enviroment]);
require('./config/express')(app, config[enviroment]);
require('./config/routes')(app);
require('./config/passport')();

app.listen(port);

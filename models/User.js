const mongoose = require('mongoose');
const encryption = require('../utils/encryption');
const propertyIsRequired = '{0} is required.';

let userSchema = new mongoose.Schema({
    username: {
        type: mongoose.Schema.Types.String,
        required: propertyIsRequired.replace('{0}', 'Username'),
        unique: true
    },
    password: {
        type: mongoose.Schema.Types.String,
        required: propertyIsRequired.replace('{0}', 'Password')
    },
    firstName: {
        type: mongoose.Schema.Types.String,
        required: propertyIsRequired.replace('{0}', 'First Name')
    },
    lastName: {
        type: mongoose.Schema.Types.String,
        required: propertyIsRequired.replace('{0}', 'Last Name')
    },
    age: {
        type: mongoose.Schema.Types.Number,
        min: [0, 'Age must be between 0 and 120'],
        max: [120, 'Age must be between 0 and 120']
    },
    gender: {
        type: mongoose.Schema.Types.String,
        enum: {
            values: ['Male', 'Female'],
            message: 'Gender should be either "Male" or "Female"'
        } 
    },
    salt : {
        type: mongoose.Schema.Types.String,
        required: true
    },
    roles: [{ type: mongoose.Schema.Types.String }],
    boughtProducts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
    createdProducts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
    createdCategories: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' }]
});

userSchema.method({
    authenticate: function (password) {
        let hashedPassword = encryption.generateHashedPassword(this.salt, password);
        if (hashedPassword === this.password){
            return true;
        }
        return false;
    }
});

const User = mongoose.model('User', userSchema);
module.exports = User;

module.exports.seedAdminUser = () => {
    User.find().then((users) => {
        if(users.length == 0) {
            let salt = encryption.generateSalt();
            let hashedPassword = encryption.generateHashedPassword(salt, 'admin');
            User.create({
                username: 'admin',
                password: hashedPassword,
                salt: salt,
                firstName: 'admin',
                lastName: 'admin',
                age: 30,
                gender: 'Male',
                roles: ['Admin']
            }).then(() => {
                console.log('Successfully seeded admin user.');
            }).catch((err) => {
                console.warn(err);
            });
        }
    });
}
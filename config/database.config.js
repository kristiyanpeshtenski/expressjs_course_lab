const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
module.exports = (config) => {
    mongoose.connect(config.connectionString);
    let database = mongoose.connection;

    database.once('open', (err) => {
        if (err){
            console.warn(err);
            return;
        }
        console.log('Connected!');
    });
    database.on('error', (err) => {
        console.warn(err);
    });

    require('../models/Product');
    require('../models/Category');
    require('../models/User').seedAdminUser();
}
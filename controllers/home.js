const fs = require('fs');
const path = require('path');
const Product = require('../models/Product');

module.exports.index = (req, res) => {
    let queryData = req.query;
    let data = {};

    Product.find({ buyer: null }).populate('category').then((products) => {
        if (queryData.query) {
            products = products.filter(p => p.name.toLowerCase().includes(queryData.query));
        }
        if(queryData.error) data.error = queryData.error;
        if(queryData.success) data.success = queryData.success;
        data.products = products;
        res.render('home/index', data);
    });
}
const fs = require('fs');
const path = require('path');

const Product = require('../models/Product');
const Category = require('../models/Category');
const User = require('../models/User');

module.exports.addGet = (req, res) => {
    Category.find().select('_id, name').then((categories) => {
        res.render('product/add', { categories: categories })
    });
}

module.exports.addPost = (req, res) => {
    let productObj = req.body;
    productObj.image = '\\' + req.file.path;
    productObj.creator = req.user._id;
    Product.create(productObj).then((product) => {
        Category.findById(product.category).then((category) => {
            category.products.push(product._id);
            category.save().then(() => {
                req.user.createdProducts.push(product._id);
                req.user.save().then(() => {
                    res.redirect(`/?success=${encodeURIComponent('Successfully added new product.')}`);
                });
            });
        });
    });
}

module.exports.editGet = (req, res) => {
    productId = req.params.product;
    let data = {};
    Product.findById(productId).then((product) => {
        if(!product){
            res.sendStatus(404);
            return;
        }
        if(product.creator == req.user._id || req.user.roles.indexOf('Admin') >= 0){
            data.product = product;
            Category.find().then((categories) => {
                for (let i = 0; i  < categories.length; i++) {
                    if(categories[i]._id.toString() == product.category.toString()) {
                        categories[i].selected = 'true';
                        break;
                    }
                }
                data.categories = categories;
                res.render('product/edit', data);
            });
        } else {
            res.redirect(`/?error=${encodeURIComponent('Only creator of the product can edit it!')}`);
            return;
        }
    });
}

module.exports.editPost = (req, res) => {
    let id = req.params.product;
    let editedProduct = req.body;

    Product.findById(id).then((product) => {
        if(!product){
            res.redirect(`/?error=${encodeURIComponent('error=Product was not found!')}`);
            return;
        }
        if(product.creator == req.user._id || req.user.roles.indexOf('Admin') >= 0){
            product.name = editedProduct.name;
            product.description = editedProduct.description;
            product.price = editedProduct.price;
            if(req.file){
                product.image = '\\' + req.file.path;
            }        
            if(product.category != editedProduct.category){
                Category.findById(product.category).then((currentCategory) => {
                    Category.findById(editedProduct.category).then((newCategory) => {
                        let index = currentCategory.products.indexOf(product.category);
                        if(index >= 0){
                            currentCategory.products.splice(index, 1);
                        }
                        currentCategory.save();
                        newCategory.products.push(product._id);
                        newCategory.save();
    
                        product.category = editedProduct.category;
                        product.save().then(() => {
                            res.redirect(`/?success=${encodeURIComponent('Product was edited successfully!')}`)
                        });
                    });
                });
            } else {
                product.save().then(() => {
                    res.redirect(`/?success=${encodeURIComponent('Product was edited successfully!')}`)
                })
            }
        } else {
            res.redirect(`/?error=${encodeURIComponent('Only creator of the product can edit it!')}`);
            return;
        }
    });
}

module.exports.deleteGet = (req, res) => {
    let id = req.params.id;
    Product.findById(id).then((product) => {
        if(!product) {
            res.redirect(`/?error=${encodeURIComponent('error=Product was not found!')}`);
            return;
        }
        if (product.creator == req.user._id || req.user.roles.indexOf('Admin') >= 0){
            res.render('product/delete', { product: product });
        } else {
            res.redirect(`/?error=${encodeURIComponent('Only creator of the product can delete it!')}`);
            return;
        }
    });
}

module.exports.deletePost = (req, res) => {
    let id = req.params.id;
    Product.findById(id).then((product) => {
        if(!product){
            res.redirect(`/?error=${encodeURIComponent('error=Product was not found!')}`);
            return;
        }
        if (product.creator == req.user._id || req.user.roles.indexOf('Admin') >= 0) {
            Category.findById(product.category).then((category) => {
                let index = category.products.indexOf(product._id);
                if(index >= 0){
                    category.products.splice(index, 1);
                }
                category.save();
                let imagePath = path.normalize(path.join(__dirname, `../${product.image}`))
                fs.unlink(imagePath, (err) => {
                    if(err){
                        console.warn(err);
                        res.redirect(`/?error=${encodeURIComponent('Fail to delete product!')}`);
                        return;
                    }
                    let productIndex = req.user.createdProducts.indexOf(product._id);
                    if(productIndex >= 0) {
                        req.user.createdProducts.splice(productIndex, 1);
                        req.user.save();
                    } 
                    Product.remove({_id: product._id}).then(() => {
                        res.redirect(`/?success=${encodeURIComponent('Product deleted successfully!')}`);
                    });
                });
            });    
        } else {
            res.redirect(`/?error=${encodeURIComponent('Only creator of the product can delete it!')}`);
            return;
        }        
    });
}

module.exports.buyGet = (req, res) => {
    let id = req.params.id;
    Product.findById(id).then((product) => {
        if(!product){
            res.redirect(`/?error=${encodeURIComponent('error=Product was not found!')}`);
            return;
        }
        res.render('product/buy', { product: product });
    });
}

module.exports.buyPost = (req, res) => {
    let productId = req.params.id;
    
    Product.findById(productId).then((product) => {
        if(product.buyer){
            let error = `error=${encodeURIComponent('Product was already bought')}`;
            res.redirect(`/?${error}`);
            return;
        }

        product.buyer = req.user._id;
        product.save().then(() => {
            req.user.boughtProducts.push(productId);
            req.user.save().then(() => {
                res.redirect('/');
                return;
            }).catch((err) => {
                console.warn(err);
                return;
            });
        });
    });
    
}
const fs = require('fs');
const path = require('path');
const url = require('url');

function getContentType(url){
    let fileType = url.substring(url.indexOf('.') + 1);
    switch (fileType) {
        case 'css':
            return 'text/css';
            break;
        case 'ico':
            return 'image/x-icon';
            break;
        case 'gif':
            return 'image/gif';
            break;
        case 'jpeg':
            return 'image/jpeg';
            break;
        case 'jpg':
            return 'image/jpg';
            break;
        default:
            return 'text/plain';
            break;
    }
}

module.exports = (req, res) => {
    req.pathname = req.pathname || url.parse(req.url).pathname;
    if (req.pathname.startsWith('/content/') && req.method == 'GET'){
        let filePath = path.normalize(
            path.join(__dirname, `..${req.pathname}`));
        fs.readFile(filePath, (err, data) => {
            if (err) {
                res.writeHead(404, {
                    'Content-Type': 'text-plain'
                });
                console.log(err);
                res.write('Resource not found!');
                res.end();
                return;
            }
            
            res.writeHead(200, {
                'Content-Type': getContentType(req.pathname)
            });

            res.write(data);
            res.end();
            return;
        });
    } else {
        return true;
    }
}